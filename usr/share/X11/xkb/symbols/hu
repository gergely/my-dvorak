partial alphanumeric_keys
xkb_symbols "dvorak" {
    name[Group1]="Hungarian (Dvorak)";
    key <LFSH> {
        type[Group1]="TWO_LEVEL",
        symbols[Group1] = [ Shift_L, Caps_Lock ]
    };
    key <RALT> {
        type[Group1]="ONE_LEVEL",
        symbols[Group1] = [ ISO_Level3_Shift ]
    };
    replace key <LVL3> {
        type[Group1] = "ONE_LEVEL",
        symbols[Group1] = [ ISO_Level3_Shift ]
    };
    modifier_map Mod5 { <LVL3> };
    replace key <CAPS> { [ Control_L ] };
    replace key <LCTL> { [ Shift_Lock ] };
    modifier_map Control { <CAPS> };
    modifier_map  Lock { <LCTL> };

//                  `             ~            ¬                     ¶
    key <TLDE>  { [ grave,        asciitilde,  notsign,              paragraph        ] };
//                  1             !            ˇ                     ¸
    key <AE01>  { [ 1,            exclam,      dead_caron,           dead_cedilla     ] };
//                  2             @            ₂                     ²
    key <AE02>  { [ 2,            at,          twosubscript,         twosuperior      ] };
//                  3             ^            ³                     ^
    key <AE03>  { [ 3,            asciicircum, threesuperior,        dead_circumflex  ] };
//                  4             $            €                     ¤
    key <AE04>  { [ 4,            dollar,      EuroSign,             currency         ] };
//                  5             °            %                     ‰
    key <AE05>  { [ 5,            percent,     degree,               U2030            ] };
//                  6             ˘            `                     ©
    key <AE06>  { [ 6,            dead_breve,  dead_grave,           copyright        ] };
//                  7             &            ~                     ®
    key <AE07>  { [ 7,            ampersand,   dead_tilde,           registered       ] };
//                  8             *            •                     ™
    key <AE08>  { [ 8,            asterisk,    enfilledcircbullet,   trademark        ] };
//                  9             (            ✓                     /
    key <AE09>  { [ 9,            parenleft,   U2713,                dead_stroke      ] };
//                  0             )            ✗                     °
    key <AE10>  { [ 0,            parenright,  U2717,                dead_abovering   ] };
//                  -             _            –                     —
    key <AE11>  { [ minus,        underscore,  endash,               emdash           ] };
//                  =             +            ×                     ±
    key <AE12>  { [ equal,        plus,        multiply,             plusminus        ] };

//                  /             ?            æ                     Æ
    key <AD01>  { [ slash,        question,    ae,                   AE               ] };
//                  ,             <            ö                     Ö
    key <AD02>  { [ comma,        less,        odiaeresis,           Odiaeresis       ] };
//                  .             >            …                     ˝
    key <AD03>  { [ period,       greater,     ellipsis,             dead_doubleacute ] };
//                  p             P            ü                     Ü
    key <AD04>  { [ p,            P,           udiaeresis,           Udiaeresis       ] };
//                  y             Y            ý                     Ý
    key <AD05>  { [ y,            Y,           yacute,               Yacute           ] };
//                  f             F            ı                     İ
    key <AD06>  { [ f,            F,           idotless,             Iabovedot        ] };
//                  g             G            ĝ                     Ĝ
    key <AD07>  { [ g,            G,           gcircumflex,          Gcircumflex      ] };
//                  c             C            ĉ                     Ĉ
    key <AD08>  { [ c,            C,           ccircumflex,          Ccircumflex      ] };
//                  r             R            ŗ                     Ŗ
    key <AD09>  { [ r,            R,           rcedilla,             Rcedilla         ] };
//                  l             L            ş                     Š
    key <AD10>  { [ l,            L,           scedilla,             Scedilla         ] };
//                  [             {            ´                     ¨
    key <AD11>  { [ bracketleft,  braceleft,   dead_acute,           dead_diaeresis   ] };
//                  ]             }            ˛                     °
    key <AD12>  { [ bracketright, braceright,  dead_ogonek,          dead_abovering   ] };

//                  a             A            á                     Á
    key <AC01>  { [ a,            A,           aacute,               Aacute           ] };
//                  o             O            ó                     Ó
    key <AC02>  { [ o,            O,           oacute,               Oacute           ] };
//                  e             E            é                     É
    key <AC03>  { [ e,            E,           eacute,               Eacute           ] };
//                  u             U            ú                     Ú
    key <AC04>  { [ u,            U,           uacute,               Uacute           ] };
//                  i             I            í                     Í
    key <AC05>  { [ i,            I,           iacute,               Iacute           ] };
//                  d             D            ð                     Ð
    key <AC06>  { [ d,            D,           eth,                  ETH              ] };
//                  h             H            ĥ                     Ĥ
    key <AC07>  { [ h,            H,           hcircumflex,          Hcircumflex      ] };
//                  t             T            þ                     Þ
    key <AC08>  { [ t,            T,           thorn,                THORN            ] };
//                  n             N            ñ                     Ñ
    key <AC09>  { [ n,            N,           ntilde,               Ntilde           ] };
//                  s             S            š                     Š
    key <AC10>  { [ s,            S,           scircumflex,          Scircumflex      ] };
//                  '             "            ′                     ″
    key <AC11>  { [ apostrophe,   quotedbl,    minutes,              seconds          ] };
//                  #             |            §                     \
    key <BKSL>  { [ numbersign,   bar,         section,              backslash        ] };

//                  \             |
    key <LSGT>  { [ backslash,    bar,         NoSymbol,             NoSymbol         ] };
//                  ;             :            ŭ                     Ŭ
    key <AB01>  { [ semicolon,    colon,       ubreve,               Ubreve           ] };
//                  q             Q            ő                     Ő
    key <AB02>  { [ q,            Q,           odoubleacute,         Odoubleacute     ] };
//                  j             J            ĵ                     Ĵ
    key <AB03>  { [ j,            J,           jcircumflex,          Jcircumflex      ] };
//                  k             K            ű                     Ű
    key <AB04>  { [ k,            K,           udoubleacute,         Udoubleacute     ] };
//                  x             X            ï                     Ï
    key <AB05>  { [ x,            X,           idiaeresis,           Idiaeresis       ] };
//                  b             B            “                     ‛
    key <AB06>  { [ b,            B,           leftdoublequotemark,  U201B            ] };
//                  m             M            „                     »
    key <AB07>  { [ m,            M,           doublelowquotemark,   guillemotright   ] };
//                  w             W            ”                     «
    key <AB08>  { [ w,            W,           rightdoublequotemark, guillemotleft    ] };
//                  v             V            ‘                     ›
    key <AB09>  { [ v,            V,           leftsinglequotemark,  U203A            ] };
//                  z             Z            ’                     ‹
    key <AB10>  { [ z,            Z,           rightsinglequotemark, U2039            ] };

    key <RWIN>  { [ Multi_key                                                         ] };

// TODO: Find a way for these to work
//    key <UP>    { [ Up,           Up,          uparrow,              NoSymbol         ] };
//    key <DOWN>  { [ Down,         Down,        downarrow,            NoSymbol         ] };
//    key <LEFT>  { [ Left,         Left,        leftarrow,            NoSymbol         ] };
//    key <RGHT>  { [ Right,        Right,       rightarrow,           NoSymbol         ] };
};
